package com.tdtri.travelutt.client.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Travel (
    @SerializedName("idTravel")
    @Expose
    var idTravel: String? = null,

    @SerializedName("nameTravel")
    @Expose
    var nameTravel: String? = null,

    @SerializedName("imageTravel")
    @Expose
    var imageTravel: String? = null,

    @SerializedName("content")
    @Expose
    var content: String? = null
)