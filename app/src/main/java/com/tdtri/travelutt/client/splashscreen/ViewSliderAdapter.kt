package com.tdtri.travelutt.client.splashscreen

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import java.util.ArrayList


class ViewSliderAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {



    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return SplashScreenFragment.newInstance(position + 1)
    }

}