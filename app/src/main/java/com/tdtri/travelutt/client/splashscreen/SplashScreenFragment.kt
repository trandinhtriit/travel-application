package com.tdtri.travelutt.client.splashscreen


import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.viewpager2.widget.ViewPager2
import com.tdtri.travelutt.R
import com.tdtri.travelutt.databinding.FragmentSplashScreenBinding


/**
 * A simple [Fragment] subclass.
 * Use the [SplashScreenFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SplashScreenFragment : Fragment() {

    private var mAdapter: ViewSliderAdapter? = null
    private var bindingView: FragmentSplashScreenBinding? = null
    var imageSlider =
        intArrayOf(R.drawable.bg_slider_one, R.drawable.bg_slider_two, R.drawable.bg_slider_three)
    private var indicator: Array<ImageView>? = null
    private var page: Int = 0


    companion object {
        private const val PAGE_NUMBER: String = "PAGE_NUMBER"

        @JvmStatic
        fun newInstance(position: Int): SplashScreenFragment {
            val fragment = SplashScreenFragment()
            val bundle = Bundle()
            bundle.putInt(PAGE_NUMBER, position)
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        bindingView = FragmentSplashScreenBinding.inflate(layoutInflater)
        bindingView?.ivSliderImage?.setBackgroundResource(
            imageSlider[arguments?.getInt(PAGE_NUMBER) ?: 0]
        )
        return bindingView?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        addAnimationView()
    }

    private fun addAnimationView() {
        val anima = ValueAnimator.ofFloat(0f, 10f)
        val mDuration = 500 //in millis

        anima.duration = mDuration.toLong()
        anima.addUpdateListener { animation ->
            bindingView?.ivNext?.translationX = animation.animatedValue as Float
        }
        anima.interpolator = OvershootInterpolator(1F)
        anima.repeatCount = Animation.INFINITE
        anima.start()
    }

    private fun initView() {
            updateIndicatorPage(0)
        indicator = arrayOf(
            bindingView!!.ivIndicatorOne,
            bindingView!!.ivIndicatorTwo,
            bindingView!!.ivIndicatorThree
        )
        mAdapter = ViewSliderAdapter(this)
        bindingView?.viewPagerSlider?.adapter = mAdapter
        bindingView?.viewPagerSlider?.currentItem = page
        updateIndicatorPage(page)

        bindingView?.viewPagerSlider?.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                page = position
                updateIndicatorPage(page)
                when (position) {
                    0 -> {
                        bindingView?.ivSliderImage?.setImageResource(R.drawable.bg_slider_one)
                    }
                    1 -> {
                        bindingView?.ivSliderImage?.setImageResource(R.drawable.bg_slider_two)
                    }
                    2 -> {
                        bindingView?.ivSliderImage?.setImageResource(R.drawable.bg_slider_three)
                    }
                }

                bindingView?.btnNext?.visibility = if (position == 2) View.GONE else View.VISIBLE
                bindingView?.btnBack?.visibility = if (position == 2) View.GONE else View.VISIBLE
                bindingView?.llIndicator?.visibility =
                    if (position == 2) View.GONE else View.VISIBLE
                bindingView?.llNext?.visibility = if (position < 2) View.GONE else View.VISIBLE
            }
        })

        bindingView?.btnNext?.setOnClickListener {
            page += 1
            bindingView?.viewPagerSlider?.setCurrentItem(page, true)
        }

        bindingView?.llNext?.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.TravelHomeFragment)
        }

        bindingView?.btnBack?.setOnClickListener {
            page -= 1
            bindingView?.viewPagerSlider?.setCurrentItem(page, true)
        }

    }

    private fun updateIndicatorPage(position: Int) {
        indicator?.let {
            for (i in it.indices) {
                it[i].setBackgroundResource(
                    if (i == position) R.drawable.bg_selected_indicator else R.drawable.bg_unselected_indicator
                )
            }
        }

    }
}


