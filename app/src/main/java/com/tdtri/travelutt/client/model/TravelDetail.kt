package com.tdtri.travelutt.client.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TravelDetail(
    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("nameTravel")
    @Expose
    var nameTravel: String? = null,

    @SerializedName("imageTravel")
    @Expose
    var imageTravel: String? = null,

    @SerializedName("content")
    @Expose
    var content: String? = null,

    @SerializedName("imageMap")
    @Expose
    var imageMap: String? = null,

    @SerializedName("imageDetail")
    @Expose
    var imageDetail: String? = null,

    @SerializedName("idTravel")
    @Expose
    var idTravel: String? = null,

    @SerializedName("Location")
    @Expose
    var location: String? = null
)