package com.tdtri.travelutt.api

import com.tdtri.travelutt.client.model.Travel
import com.tdtri.travelutt.client.model.TravelDetail
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface DataApi {

    @FormUrlEncoded
    @POST("TravelLocation.php")
    fun getDataTravel(@Field("idTravel") idFood: String?): Call<List<TravelDetail>>

    @FormUrlEncoded
    @POST("SearchTravel.php")
    fun getSearchTravel(@Field("keyword") keyword: String?): Call<List<TravelDetail>>

    @GET("Travel.php")
    fun getDataForTravel(): Call<List<Travel>>
}